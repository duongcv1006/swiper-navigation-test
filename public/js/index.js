const container = document.querySelector(".swiper-container");
const wrapper = document.querySelector(".swiper-wrapper");
const slides = document.querySelectorAll(".swiper-slide");
const prevBtn = document.querySelector(".swiper-button-prev");
const nextBtn = document.querySelector(".swiper-button-next");

let currentIndex = 0;
let isPointerDown = false;
let startPointerX = 0;
let slideWidth = 0;
let isDragging = false;

function updateSlideWidth() {
  slideWidth = slides[0].offsetWidth;
  updateSlider();
}

function updateSlider() {
  wrapper.style.transform = `translate3d(-${currentIndex * slideWidth}px, 0px, 0px)`;
  slides.forEach((slide) => {
    slide.classList.remove("active");
  });

  slides[currentIndex].classList.add("active");
  slides[currentIndex].style.minWidth = `100%`;

  prevBtn.disabled = currentIndex === 0;
  nextBtn.disabled = currentIndex === slides.length - 1;
}

function handlePointerStart(event) {
  const clientX = event.touches ? event.touches[0].clientX : event.clientX;
  startPointerX = clientX;
  isPointerDown = true;
  wrapper.style.transition = "none";
}

function handlePointerMove(event) {
  if (!isPointerDown) return;

  const clientX = event.touches ? event.touches[0].clientX : event.clientX;
  const deltaX = clientX - startPointerX;

  if (deltaX > 50 && currentIndex === 0) {
    isDragging = true;
    wrapper.style.transform = `translate3d(${deltaX - 50}px, 0px, 0px)`;
  }

  if (currentIndex === slides.length - 1) {
    isDragging = true;
    wrapper.style.transform = `translate3d(-${currentIndex * slideWidth - deltaX}px, 0px, 0px)`;
  }

  if (currentIndex === slides.length - 1) {
    isDragging = true;
    wrapper.style.transform = `translate3d(-${currentIndex * slideWidth - deltaX}px, 0px, 0px)`;
  }
}

function handlePointerEnd(event) {
  if (!isPointerDown) return;
  isPointerDown = false;

  const clientX = event.changedTouches ? event.changedTouches[0].clientX : event.clientX;
  const deltaX = clientX - startPointerX;
  wrapper.style.transition = "transform 300ms ease";

  if (isDragging && currentIndex === 0) {
    wrapper.style.transform = `translate3d(0, 0, 0)`;
    isDragging = false;
  } else {
    if (deltaX > 50) {
      currentIndex--;
    } else if (deltaX < -50 && currentIndex < slides.length - 1) {
      currentIndex++;
    }
    setTimeout(() => {
      wrapper.style.transition = "none";
      updateSlider();
    }, 300);
  }
  updateSlider();
}

function handlePointerEnd(event) {
  if (!isPointerDown) return;
  isPointerDown = false;

  const clientX = event.changedTouches ? event.changedTouches[0].clientX : event.clientX;
  const deltaX = clientX - startPointerX;
  wrapper.style.transition = "transform 300ms ease";

  if (isDragging && currentIndex === 0) {
    wrapper.style.transform = `translate3d(0, 0, 0)`;
    isDragging = false;
  } else {
    if (deltaX > 50) {
      currentIndex--;
    } else if (deltaX < -50 && currentIndex < slides.length - 1) {
      currentIndex++;
    }
    setTimeout(() => {
      wrapper.style.transition = "none";
      updateSlider();
    }, 300);
  }
  updateSlider();
}

prevBtn.addEventListener("click", () => {
  if (currentIndex > 0) {
    wrapper.style.transition = null;
    currentIndex--;
    updateSlider();
  }
});

nextBtn.addEventListener("click", () => {
  if (currentIndex < slides.length - 1) {
    wrapper.style.transition = null;
    currentIndex++;
    updateSlider();
  }
});

container.addEventListener("mousedown", handlePointerStart);
container.addEventListener("mousemove", handlePointerMove);
container.addEventListener("mouseup", handlePointerEnd);

container.addEventListener("touchstart", handlePointerStart);
container.addEventListener("touchmove", handlePointerMove);
container.addEventListener("touchend", handlePointerEnd);

window.addEventListener("load", updateSlideWidth);
window.addEventListener("resize", updateSlideWidth);

updateSlider();
